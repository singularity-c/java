public class JavaOperator {
    public static void main(String[] args) {
//        System.out.println(5/2);//2
        //结构不为小数
//        System.out.println(5.0/2);//2.5
//        System.out.println(5/2.0);//2.5
        //先转float
//        System.out.println((float) 5/2);//2.5
//        System.out.println(5/(float)2);//2.5
//        System.out.println((float)(5/2));//2.0

//        System.out.println(10%3);//1
//        System.out.println(-10%3);//-1
//        System.out.println(10%-3);//1
//        System.out.println(-10%-3);//-1
        //用托式运算得结果

//        System.out.println(10/0);
        //java中被除数不能为零，不然会算数报错     :/ by zero
        //会告诉你在第几行代码出现错误
//        int a = 10;
        //a = a + 1;
//        a += 1;
//        System.out.println(a);
//        double d = 12.5;
//        a += d;//他是可以自动进行类型转换的

//        int a = 10;
//        int b = 20;
//        System.out.println(a==b);
//        System.out.println(a>=b);
//        System.out.println(a<=b);
//        System.out.println(a!=b);
        //判断语句只有真和假 false和true
        int a = 1;
        int b = 2;
        System.out.println(a>b);
        //ystem.out.println(a && 1);//报错，因为a和b都是整数
    }
}
