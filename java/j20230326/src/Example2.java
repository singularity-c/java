import java.util.Scanner;

public class Example2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入用电量");
        int number = scanner.nextInt();
        Electricity electricity = new Electricity(number);
        electricity.main();
    }
}

class Electricity{
    private int electricity;
    public Electricity(int electricity) {
        this.electricity = electricity;
    }
    void main() {
        double a = 0;
        double b = 0;
        double c = 0;

        if (electricity <= 90) {
            a = low(electricity);
        } else if (electricity <= 150) {
            b = low(90) + middle(electricity - 90);
        } else {
            c = low(90) + middle(60) + high(electricity-150);
        }
        System.out.println(a + b + c);
    }
    private double low(int electricitylow){
        return electricitylow * 0.6;
    }
    private double middle(int electricitymiddle){
        return electricitymiddle * 1.1;
    }
    private double high(int electricityhigh){
        return electricityhigh * 1.7;
    }
}

