package niuke;


class Animal{

    public String name;
    public int age ;
    public void eat(){
        System.out.println(name+"正在吃食物");
    }
}

class Cat extends Animal{

    Cat(String name , int age){
        super.name = name;
        super.age = age;
    }
    public void eat(){
        System.out.println(name+ "正在吃猫粮");
    }
    public void catchMouse(){
        System.out.println(name+ "正在抓老鼠");
    }
}
class Dog extends Animal{

    Dog(String name , int age){
        super.name = name;
        super.age = age;
    }
    public void eat(){
        System.out.println(name+ "正在吃狗粮");
    }
}

public class Test {
    //向上转型
    public static void shang(Animal animal){
        animal.eat();
    }

    //3.方法返回 向上转型
    public static Animal func(){
        return new Cat("jake",20);
    }
    //通过方法传参   过程中向上转型

    public static void main2(String[] args) {
        Dog dog = new Dog("Tom",18);
        shang(dog);
        Cat cat = new Cat("jake",20);
        shang(cat);
    }

    //方法一  1.父类=new 子类
    public static void main1(String[] args) {
        Animal animal1 = new Dog("Tom" , 18);
        animal1.eat();
    }



    //向下转型
    public static void main4(String[] args) {
        Animal animal2 = new Cat("Tom" , 18);
        Cat cat =(Cat)animal2;//向下转型
        cat.catchMouse();
    }

    //可能会出现类型转换异常问题
    public static void main(String[] args) {
        Animal animal2 = new Dog("Tom" , 18);
        if(animal2 instanceof Cat){//需要加判断  是否引用猫这个类型
            Cat cat =(Cat)animal2;//向下转型
            cat.catchMouse();
        }
    }
}
