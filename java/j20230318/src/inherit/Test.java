package inherit;

class A{

}
class b extends A{

}
class C extends b{
    int as=11;
    C(){
        System.out.println(1);
    }
    {
        System.out.println("动态代码块");
        int a = 10;
    }
    public void chongxie(int a){
        System.out.println(a);
    }
    static{
        System.out.println("静态代码块");
         String class1 = "20216";
    }
}



class Animal{
    public String name;
    public int age;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }


    public void eat(){
        System.out.println("正在吃饭！ ");
    }

}

//子类叫派生类
//父类叫基类或超类
//当子类继承父类后就会把属性和方法全部继承
class Dog extends Animal{//Dog 继承了 Animal 类
    //傻狗是狗的属性
    public boolean silly;
    public Dog(String name,int age,boolean silly){
        //1.先帮助父类部分初始化 必须放到第一行
        super(name,age);
        this.silly = silly;
    }

    public void barks(){
        System.out.println(name + "汪汪叫! " + " 年龄：" + age);
    }
}
class Cat extends Animal{

    public Cat(){
        this("咪咪",18);
    }
    public Cat(String name , int age){
        super(name , age);
    }
    public void catchMouse(){
        System.out.println(name + "正在抓老鼠");
    }
}
public class Test {

    public static void main(String[] args) {
        C c = new C();
        System.out.println("========");
        C b = new C();
        System.out.println();
    }

    public static void main1(String[] args) {
        Dog dog = new Dog("yom",8,false);
/*        dog.name = "旺财";
        dog.age = 10;*/
        dog.barks();
        Cat cat = new Cat();
        cat.catchMouse();


    }

    public static void main2(String[] args) {
        int[] salary = {1000,2000,3000,4000};
        double mix=salary[0];
        double max=salary[0];
        double sum = 0;
        for(int i = 0;i < salary.length; i++){
            if(mix > salary[i]){
                mix = salary[i];
            }
            if(max < salary[i]){
                max = salary[i];
            }
        }
        for(int j = 0;j < salary.length; j++){
            sum += salary[j];
        }
        sum = sum - mix - max;
        System.out.println(sum/(salary.length-2));
//        return sum/(salary.length-2);
    }
}
