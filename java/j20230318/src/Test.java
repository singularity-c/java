class OuterClass{

    public int num = 1;
    int num2 = 2;
    public static int num3 = 3;

    public void test3(){
        System.out.println("OuterClass :: test3");
    }

    /**
     * 实例内部类
     * 1.如何获取实例内部类的对象
     OuterClass.InnerClass innerClass=outerClass.new InnerClass();
     * 2.实例内部类当中 不能有静态成员变量，如果非要定义，那么只能被static final修饰的
     * 3.在实例内部类当中 获取外部类 this : OuterClass.this.num
     */
    class InnerClass{
        public int num = 111;
        public int num4 = 4;
        private int a;
        int b;
        int num5 = 5;
        //public static int num6;//报错，无法执行
        public static final int num7 = 6;

        @Override
        public String toString() {
            return "InnerClass{" +
                    "num=" + num +
                    ", num4=" + num4 +
                    ", num5=" + num5 +
                    '}';
        }

        public void test2(){
            System.out.println("InnerClass :: test2()");
            System.out.println(OuterClass.this.num);
            System.out.println(num2);
            System.out.println(num3);
            System.out.println(num4);
            System.out.println(num5);
            System.out.println(num7);

        }

        public  void test3() {
            //一般定义常量会用大写
            final int SIZE = 10;//定义常量，常量是在程序的编译中定义的
            // 一旦初始化，就不能再修改了

        }
    }
}
public class Test {
    public static void main(String[] args) {
        OuterClass outerClass = new OuterClass();
        OuterClass.InnerClass innerClass=outerClass.new InnerClass();
        innerClass.test2();
        outerClass.toString();
    }
}
