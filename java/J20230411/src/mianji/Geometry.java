package mianji;

public abstract class Geometry {
    public abstract double getArea();
}
