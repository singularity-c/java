package mianji;

public class MainClass {
    public static void main(String[] args) {
        Student zhang = new Student();
        double area = zhang.area(new Rect(2,3),new Circle(5.2),new Circle(12));
        System.out.println("两个圆和一个矩形的面积和:\n%10.3f");
        System.out.println(area);
    }
}
