package user;

import book.Booklist;
import operations.IOPeration;

public abstract class User {
    protected String userName;
    protected IOPeration[] ioPerations;
    public User(String userName){
        this.userName=userName;
    }

    public abstract int meau();

    public void Function(int theseveral,Booklist booklist){
        ioPerations[theseveral].book(booklist);
    }
}
