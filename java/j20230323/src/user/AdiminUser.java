package user;

import operations.*;

import java.util.Scanner;

public class AdiminUser extends User{
    public AdiminUser(String name){
        super(name);
        this.ioPerations = new IOPeration[] {
                new ExitBook(),

                new InquireBook(),
                new AddBook(),
                new DelBook(),
                new DisplayBook()
        };
    }


    @Override
    public int meau() {
        System.out.println("欢迎用户 "+ userName +" 进入图书管理小练习" );
        System.out.println("***************************************");
        System.out.println("1.查找图书");
        System.out.println("2.增加图书");
        System.out.println("3.删除图书");
        System.out.println("4.显示图书");
        System.out.println("0.退出系统");
        System.out.println("***************************************");
        Scanner scanner = new Scanner(System.in);
        int theseveral = scanner.nextByte();
        return theseveral;
    }
}
