package user;

import operations.*;

import java.util.Scanner;

public class NormalUser extends User{
     public NormalUser(String name){
        super(name);
         this.ioPerations = new IOPeration[] {
                 new ExitBook(),
                 new InquireBook(),
                 new BorrwBook(),
                 new ReturnBook()
         };
     } 

    @Override
    public int meau() {
        System.out.println("欢迎用户 "+ userName +" 进入图书管理小练习" );
        System.out.println("***************************************");
        System.out.println("1.查找图书");
        System.out.println("2.借阅图书");
        System.out.println("3.归还图书");
        System.out.println("0.退出系统");
        System.out.println("***************************************");
        Scanner scanner = new Scanner(System.in);
        int theseveral = scanner.nextByte();
        return theseveral;
     }
}
