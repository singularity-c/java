import book.Booklist;
import user.AdiminUser;
import user.NormalUser;
import user.User;

import java.util.Scanner;

public class Main {
    public static User func(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入姓名");
        String name =scanner.nextLine();

        System.out.println("1.管理员  2.游客");
        int youke =scanner.nextInt();
        if(youke == 1){
            return new AdiminUser(name);
        }else {
            return new NormalUser(name);
        }
    }

    public static void main(String[] args) {
        //1.准备数据
        Booklist booklist = new Booklist();
        //2.登录
        User user =func();
        while (true){
        user.Function(user.meau(), booklist);
        }
    }
}