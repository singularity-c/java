package operations;

import book.Book;
import book.Booklist;

public class DisplayBook implements IOPeration{//显示所有图书
    public void book(Booklist booklist) {
        System.out.println("显示图书！");
        for(int i = 0;i < booklist.getBookAge();i++){
            Book books = booklist.getBook(i);
            System.out.println(books);
        }
        System.out.println("一共有"+ booklist.getBookAge() +"本图书");
    }
}
