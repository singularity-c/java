package operations;

import book.Book;
import book.Booklist;

import java.util.Scanner;

public class BorrwBook implements IOPeration{//借阅图书
    public void book(Booklist booklist) {
        System.out.println("借阅图书！");
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你要借阅图书的名字");
        String name = scanner.nextLine();

        for(int i = 0 ; i < booklist.getBookAge(); i++) {
            Book book = booklist.getBook(i);
            if (name.equals(book.getName())) {
                if (book.isBorrowed()==false){
                    book.setBorrowed(true);
                    System.out.println(book);
                    System.out.println("借阅成功");
                    return;
                }else{
                    System.out.println(book);
                    System.out.println("图书已被借出");
                    return;
                }

            }
        }
        System.out.println("未找到您输入的图书");
    }
}
