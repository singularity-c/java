package operations;

import book.Booklist;

public interface IOPeration {
    void book(Booklist booklist);
}
