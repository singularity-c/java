package operations;

import book.Book;
import book.Booklist;

import java.util.Scanner;

public class AddBook implements IOPeration{//添加图书
    public void book(Booklist booklist) {
        Scanner m = new Scanner(System.in);
        System.out.println("增加图书！");
        System.out.println("请输入新增图书名");
        String name = m.nextLine();
        System.out.println("请输入新增图书作者");
        String author = m.nextLine();
        System.out.println("请输入新增图价格");
        int price = m.nextInt();
        m.nextLine();
        System.out.println("请输入新增图出类型");
        String type = m.nextLine();
        System.out.println("请输入新增图出版社");
        String piblisher = m.nextLine();
        booklist.ADD(name,author,price,type,piblisher);
        System.out.println("一共有"+ booklist.getBookAge() +"本图书");
    }
}
