package operations;

import book.Book;
import book.Booklist;

import java.util.Scanner;

public class ReturnBook implements IOPeration{//归还图书
    public void book(Booklist booklist) {
        System.out.println("归还图书！");
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你要归还图书的名字");
        String name = scanner.nextLine();
        for(int i = 0 ; i < booklist.getBookAge(); i++) {
            Book book = booklist.getBook(i);
            if (name.equals(book.getName())) {
                if (book.isBorrowed()==true){
                    book.setBorrowed(false);
                    System.out.println(book);
                    System.out.println("归还成功");
                    return;
                }else{
                    System.out.println(book);
                    System.out.println("图书未被借出");
                    return;
                }
            }
        }
        System.out.println("未找到您输入的图书");
    }
}
