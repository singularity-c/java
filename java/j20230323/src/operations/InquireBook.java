package operations;

import book.Book;
import book.Booklist;

import java.util.Scanner;

public class InquireBook implements IOPeration{//查询图书

    @Override
    public void book(Booklist booklist) {
        System.out.println("查询图书！");
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你要查询图书的名字");
        String name = scanner.nextLine();
        for(int i = 0 ; i < booklist.getBookAge(); i++) {
            Book book = booklist.getBook(i);
            if (name.equals(book.getName())) {
                System.out.println(book);
                return;
            }
        }
        System.out.println("没找到输入的书籍");
    }
}
