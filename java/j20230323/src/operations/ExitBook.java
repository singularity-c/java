package operations;

import book.Booklist;

public class ExitBook implements IOPeration{//推出系统
    public void book(Booklist booklist) {
        System.out.println("退出系统！");
        System.exit(0);
    }
}
