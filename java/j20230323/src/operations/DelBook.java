package operations;

import book.Book;
import book.Booklist;

import java.util.Scanner;

public class DelBook implements IOPeration{//删除图书
    public void book(Booklist booklist) {
        System.out.println("删除图书！");
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你要删除图书的名字");
        String name = scanner.nextLine();

        int Dlebook = -1;
        for(int i = 0 ; i < booklist.getBookAge(); i++) {
            Book book = booklist.getBook(i);
            if (name.equals(book.getName())) {
                Dlebook = i;
                booklist.setBookAge(booklist.getBookAge()-1);
                break;
            }
        }

        if (Dlebook == -1){
            System.out.println("没找到输入的书籍");
            return;
        }

        for (int j = Dlebook; j < booklist.getBookAge(); j++) {
            Book book = booklist.getBook(j+1);
            booklist.setBook(j,book) ;
        }
        booklist.setBook(booklist.getBookAge(),null);
    }
}
