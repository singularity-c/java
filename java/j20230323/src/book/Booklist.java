package book;

public class Booklist {
    Book[] books = new Book[10];
    protected int bookAge;//记录还有多少本书

    //通过构造方法先准备三本书
    public Booklist(){
        books[0] =new Book("三国演义","罗贯中",68,"小说","北京教育出版社") ;
        books[1] =new Book("水浒传","施耐庵",88,"小说","北京教育出版社") ;
        books[2] =new Book("西游记","吴承恩",78,"小说","人民教育出版社") ;
        this.bookAge = 3;
    }

    public int getBookAge() {
        return bookAge;
    }

    public void setBookAge(int bookAge) {
        this.bookAge = bookAge;
    }

    public void ADD(String name,String author,int price,String type,String piblisher){
        books[bookAge] = new Book(name,author,price,type,piblisher);
        this.bookAge++;
    }



    public Book getBook(int i){
        return books[i];
    }
    public void setBook(int i , Book book){
        this.books[i] = book;
    }
}
