package book;

public class Book {
    private String name;//书名
    private String author;//作者
    private int price;//数的价格
    private String type;//书的类型
    private String publisher;//书的出版社
    private boolean isBorrowed;//是否被借出


    public Book(String name, String author, int price, String type, String publisher) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.type = type;
        this.publisher = publisher;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public boolean isBorrowed() {
        return isBorrowed;
    }

    public void setBorrowed(boolean borrowed) {
        isBorrowed = borrowed;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", price=" + price +
                ", type='" + type + '\'' +
                ", publisher='" + publisher + '\'' +
                ", isBorrowed=" + isBorrowed +
                '}';
    }
}
