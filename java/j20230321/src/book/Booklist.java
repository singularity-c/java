package book;

public class Booklist {
    public Book[] books = new Book[10];
    private int usedSize;//来存储现在放了几本书


    /**
     * 事先通过构造方法给数组放了三本书
     */
    public Booklist(){
        books[0] =new Book("三国演义","罗贯中",68,"小说") ;
        books[1] =new Book("水浒传","施耐庵",88,"小说") ;
        books[2] =new Book("西游记","吴承恩",78,"小说") ;
        this.usedSize = 3;
    }

    public int getUsedSize() {
        return usedSize;
    }

    public void setUsedSize(int usedSize) {
        this.usedSize = usedSize;
    }

    public Book getPos(int pos){
        return books[pos];
    }

    public void setBooks(Book book,int pos) {
        books[pos]=book;
    }

    public Book setPos(String name, String author, int price, String type){
       books[usedSize] = new Book(name,author,price,type);
       return  books[usedSize];
    }




}
