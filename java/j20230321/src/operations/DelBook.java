package operations;

import book.Book;
import book.Booklist;

import java.util.Scanner;

public class DelBook implements IOPeration{
    public void wark(Booklist booklist){
        System.out.println("删除图书");
        System.out.println("请输入您要删除图书的名字");
        int index = -1;
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();

        int currentSize = booklist.getUsedSize();
        for (int i = 0; i < currentSize ; i++) {
            Book book = booklist.getPos(i);
            if (name.equals(book.getName())){
                index = i;
                break;
            }
        }

/*        if(index == currentSize){
            booklist.setBooks(null,currentSize);
            System.out.println("图书已被删除");
            return;
        }*/

        if (index == -1){
            System.out.println("没有这本书");
            return;
        }

        for (int j = index; j < currentSize-1; j++) {
            Book book = booklist.getPos(j+1);
            booklist.setBooks(book,j);
        }

        booklist.setBooks(null,currentSize-1);
        booklist.setUsedSize(currentSize-1);
    }
}
