package operations;

import book.Book;
import book.Booklist;

import java.util.Scanner;

public class AddBook implements IOPeration{

    public void wark(Booklist booklist){
        System.out.println("增加图书");

        Scanner scanner = new Scanner(System.in);

        System.out.println("请输入您要新增图书的名字");
        String name = scanner.nextLine();
        System.out.println("请输入您要新增图书的作者的名字");
        String author = scanner.nextLine();
        System.out.println("请输入您要新增图书的价格");
        int price = scanner.nextInt();
        scanner.nextLine();
        System.out.println("请输入您要新增图书的类型");
        String type = scanner.nextLine();


        Book book = booklist.setPos(name,author,price,type);
        book.setBorrowed(false);
        System.out.println("图书储存成功");

        int usedSize = booklist.getUsedSize();
        booklist.setUsedSize(usedSize+1);
        usedSize++;
        System.out.println("一共有"+usedSize+"本图书");
    }
}
