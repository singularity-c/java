package operations;

import book.Booklist;

public class ExitBook implements IOPeration{
    @Override
    public void wark(Booklist booklist) {
        System.out.println("退出系统！");
        System.exit(0);
    }
}
