package operations;

import book.Book;
import book.Booklist;

import java.util.Scanner;

public class BorrwBook implements IOPeration{
    @Override
    public void wark(Booklist booklist) {
        System.out.println("借阅书籍");
        System.out.println("请输入您要借阅图书的名字");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();

        int currentSize = booklist.getUsedSize();
        for (int i = 0; i < currentSize ; i++) {
            Book book = booklist.getPos(i);
            if (name.equals(book.getName())){
                System.out.println("找到了这本书");
                if(book.isBorrowed() == false){
                    book.setBorrowed(true);
                    System.out.println("借书成功");
                    System.out.println(book);
                    return;
                } else if (book.isBorrowed() == true) {
                    System.out.println("图书已被借出");
                    System.out.println(book);
                    return;
                }
            }
        }
        System.out.println("没有这本书");
    }
}

