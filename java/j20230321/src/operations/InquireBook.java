package operations;

import book.Book;
import book.Booklist;

import java.util.Scanner;

public class InquireBook implements IOPeration{

    public void wark(Booklist booklist){
        System.out.println("查询图书");
        System.out.println("请输入您要查询图书的名字");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();

        int currentSize = booklist.getUsedSize();
        for (int i = 0; i < currentSize ; i++) {
            Book book = booklist.getPos(i);

            if (name.equals(book.getName())){
                System.out.println("找到了这本书");
                System.out.println(book);
                return;
            }
        }
        System.out.println("没有这本书");
    }
}
