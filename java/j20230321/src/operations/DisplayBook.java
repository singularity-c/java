package operations;

import book.Book;
import book.Booklist;

import java.util.Scanner;

public class DisplayBook implements IOPeration{

    @Override
    public void wark(Booklist booklist) {
        System.out.println("显示所有图书");
        int currentSize = booklist.getUsedSize();
        for (int i = 0; i < currentSize ; i++) {
            Book book = booklist.getPos(i);
            System.out.println(book);
        }
        System.out.println("图书共"+currentSize+"本显示完成");
    }
}
