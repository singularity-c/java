import book.Booklist;
import user.AdiminUser;
import user.NormalUser;
import user.User;

import java.util.Scanner;

public class Main {

    //利用向上转型来确定是什么对象
    public static User login(){
        System.out.println("欢迎来到图书管理小练习");
        System.out.println("请输入您的姓名");
        Scanner scanner = new Scanner(System.in);
        String userName = scanner.nextLine();
        System.out.println("请输入您的身份： 1.管理员   0.普通用户");
        int choice = scanner.nextInt();
        if(choice == 1){
            return new AdiminUser(userName);
        }else {
            return new NormalUser(userName);
        }

//        return choice;
    }
    public static void main(String[] args) {
        //0.准备数据
        Booklist booklist = new Booklist();

        //1.登录
        User user = login();
        while (true) {
            int choice = user.menu();
            user.doOperation(choice, booklist);
        }



    }
}