package user;

import operations.*;

import java.util.Scanner;

public class AdiminUser extends User{
    public AdiminUser(String name) {
        super(name);

        this.ioPerations = new IOPeration[]{
                new ExitBook(),
                new InquireBook(),
                new AddBook(),
                new DelBook(),
                new DisplayBook()
        };
    }
    public int menu(){
        System.out.println("***************************************");
        System.out.println("hello " + name + " 欢迎来到图书小练习");
        System.out.println("1.查找图书");
        System.out.println("2.新增图书");
        System.out.println("3.删除图书");
        System.out.println("4.显示图书");
        System.out.println("0.退出系统");
        System.out.println("***************************************");
        System.out.println("请输入您的操作：");
        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();
        return choice;
    }
}
