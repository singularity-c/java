package user;

import book.Booklist;
import operations.IOPeration;

public abstract class User {
    protected String name;
    protected IOPeration[] ioPerations;//只是定义数组没有初始化 内存都没分配

    public User(String name) {
        this.name = name;

    }
    public abstract int menu();

    public void doOperation(int chioce, Booklist booklist){
        ioPerations[chioce].wark(booklist);
    }


}

