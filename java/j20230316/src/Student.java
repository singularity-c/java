import java.util.*;

class Students{
    public String name;
    public int age;

    public static String classes = "20216";


    //调用类会先调用静态代码块，再调用实例代码块
    {
        System.out.println("这是实例代码块！");
    }
    //静态代码块只会执行一次
    static {
        System.out.println("这是静态代码块！");
    }



    public Students(){
//        this("zmy",668);//必须放在构造方法，第一行
        System.out.println("不带参数构造方法");
    }
    public Students(String name,int age){
        //this()  不能循环调用
        this.name = name;
        this.age = age;
//        System.out.println("第二个带参数构造方法");
    }
    public static void jingtai(){
       // this.daying();//在静态方法中无法直接访问非静态的成员和方法
        System.out.println("静态方法");
    }


/*    public void setStduent(String name,int age){
        this.name = name;
        this.age = age;
    }*/
    public void daying(){
        System.out.println("姓名：" + this.name);
        System.out.println("年龄：" + this.age);
        System.out.println("班级：" + classes);
        System.out.println();
    }
}

public class Student {

    public static void main(String[] args) {
        Students student = new Students();
    }

    public static void main2(String[] args) {
//        Students student3 = new Students();

        Students student1 = new Students("朱命阳",668);
        Students student2 = new Students("小明",18);

        Students.classes = "java";
//        student1.setStduent("朱命阳",668);
//        student2.setStduent("小明",18);

        student1.daying();

        student2.daying();

        Scanner shu = new Scanner(System.in);
        int a = shu.nextInt();

        System.out.println(a);


    }
}
