import java.util.Scanner;

public class Main1 {

    public static void main(String[] args) {
        Person p = new Person();
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            int age = scanner.nextInt();
            p.setAge(age);
            System.out.println(p.getAge());
        }
    }

}

class Person {
    public int age;

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        if(age>200){
            return 200;
        }else if(age<0){
            return 0;
        }else{
            return age;
        }
    }

    //write your code here......


}
