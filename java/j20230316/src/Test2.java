
class OuterClass2{
    public int data1 = 1;
    public static int data2 = 2;
    int data3 = 3;


    class InnerClass2{
        public int data4 = 4;
        public static final int data5 = 5;
        int data6 = 6;

        public void func(){
            System.out.println(data1);
            System.out.println(data2);
            System.out.println(data3);
            System.out.println(data4);
            System.out.println(data5);
            System.out.println(data6);

        }

    }



}

public class Test2 {
    public static void main(String[] args) {
        OuterClass2 outerClass1 = new OuterClass2();
        OuterClass2.InnerClass2 innerClass2 =outerClass1.new InnerClass2();
        innerClass2.func();
    }
}

