import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

interface 唱 {
    //接口中的方法常量默认被static final修饰
    //static final void chang();
    void 唱();
}
interface 跳{
    void 跳();
}
interface rap extends 唱,跳{

    void rep();
    void 篮球();
}

abstract class nameAge implements rap{
    protected String name;
    protected int age;

    public void xieru(String name,int age){
        this.name=name;
        this.age = age;
    }

    @Override
    public void 唱() {

    }

    @Override
    public void 跳() {

    }

    @Override
    public void rep() {

    }

    @Override
    public void 篮球() {

    }
    public void kun(){
        System.out.println("大家好我是练习时长"+ age +"全名制作人"+ name +"我会");
    }
}

class 坤坤 extends nameAge implements rap{

    坤坤(String name,int age){
        super.name=name;
        super.age = age;
    }

    public void kun(){
        System.out.println("大家好我是练习时长"+ age +"全名制作人"+ name +"我会");
    }
    @Override
    public void 唱() {
        System.out.print(" 唱 ");
    }

    @Override
    public void 跳() {
        System.out.print("跳 ");
    }

    @Override
    public void rep() {
        System.out.print("rap ");
    }

    @Override
    public void 篮球() {
        System.out.print("篮球 ");
    }
}

class 叶豪方 extends nameAge implements 唱,跳{
    叶豪方(String name,int age){
        super.name=name;
        super.age = age;
    }

    public void kun(){
        System.out.println("大家好我是代码练习时长"+ age +"个月代码制作人"+ name +"我会");
    }

    public void 唱() {
        System.out.print(" c++ ");
    }

    @Override
    public void 跳() {
        System.out.print(" linux ");
    }
}

class 徐春雷 extends nameAge implements 唱,跳{
    徐春雷(String name,int age){
        super.name=name;
        super.age = age;
    }

    public void kun(){
        System.out.println("大家好我是代码练习时长"+ age +"个月代码制作人"+ name +"我会");
    }

    public void 唱() {
        System.out.print(" python ");
    }

    @Override
    public void 跳() {
        System.out.print("html css javascript ");
    }
}
class 戴秉良 extends nameAge implements 唱,跳{
    戴秉良(String name,int age){
        super.name=name;
        super.age = age;
    }

    public void kun(){
        System.out.println("大家好我是干饭练习时常"+ age +"个月干饭人"+ name +"我会");
    }

    public void 唱() {
        System.out.print(" 吃饭 ");
    }

    @Override
    public void 跳() {
        System.out.print("吃很多饭 ");
    }
}

public class Interfacekun {

    public static void 真爱(nameAge kk){
        kk.kun();
        kk.唱();
        kk.跳();
        kk.rep();
        kk.篮球();
        System.out.println();
        System.out.println();
    }
    public static void main(String[] args) {
        真爱(new 坤坤("蔡徐坤",25));
        真爱(new 叶豪方("叶豪方",8));
        真爱(new 徐春雷("徐春雷",5));
        真爱(new 戴秉良("戴秉良",36));

    }
}
