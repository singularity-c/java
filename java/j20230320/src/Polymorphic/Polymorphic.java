package Polymorphic;

class 画图{
    public void 画图(){
        System.out.println("画图");
    }
}
class 画矩形 extends 画图{
    public void 画图(){
        System.out.println("画矩形 ");
    }
}

class 画圆 extends 画图{
    public void 画园(){
        System.out.println("画圆");
    }
}
class 画花 extends 画图{

    public void 画图(){
        System.out.println("❀");
    }
}


public class Polymorphic {

    public static void 画画(画图 huatu){
        huatu.画图();
    }


    public static void main(String[] args) {
        画图[] huatu = {new 画矩形(),new 画圆(),new 画花(),new 画矩形(),new 画圆()};
        for (画图 x:huatu) {
            x.画图();

        }
    }
/*    public static void main(String[] args) {
        画画(new 画圆());
        画画(new 画矩形());
        画画(new 画花());

    }*/
}
