package clone;

class People implements Cloneable{
    public double money = 12.5;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

class Student implements Cloneable{

    People people = new People();

    public int id;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Student tmp = (Student) super.clone();
        tmp.people = (People) this.people.clone();
        return tmp;
        //        return super.clone();
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                '}';
    }
}
public class Test  {
    public static void main(String[] args) throws CloneNotSupportedException  {
        Student student = new Student();
        student.id = 99;
        System.out.println(student.people.money);
        Student student1 = (Student)student.clone();
        student.people.money = 18;
        System.out.println(student.people.money);
        System.out.println(student1.people.money);
        System.out.println(student.id);
        System.out.println(student1.id);
    }
}
