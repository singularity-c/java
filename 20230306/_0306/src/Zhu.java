import java.sql.SQLOutput;

public class Zhu {
    public static void mai4(String[] args) {
        //局部变量4个字节
        int a=10;
        System.out.println(a);
        //Integer->把他当成"一个int的plas版本" 专业术语：包装类
        //MAX_VALUE 最大值   MIN_VALUE 最小值
        System.out.println(Integer.MAX_VALUE);//Integer.MAX_VALUE  这样能输出int的最大值
        System.out.println(Integer.MIN_VALUE);//这样能 出int的最小值
        int c=10;
        System.out.println(c);
        //如果定义的局部变量没有初始化，在使用之前一定要初始化 没有初始化会报错
    }

    public static void main3(String[] args) {
        /*
        8个字节-》64比特位-》63比特-》-2^63--(2^63）-1
         */
        long a=10;
        System.out.println(a);
        System.out.println(Long.MAX_VALUE);
        System.out.println(Long.MIN_VALUE);

        long b=10L;//表示是长整型的10
        System.out.println(b);
    }

    public static void main7(String[] args) {
       /*
       短整形2个字节=》16->15   -2^15-(2^15)-1
        */
        short sh=10;
        System.out.println(sh);
        System.out.println(Short.MAX_VALUE);
        System.out.println(Short.MIN_VALUE);
    }

    public static void main5(String[] args) {
        /*
        字节类型->1个字节-》8-》-2^7  -  (2^7)-1
        -128 - 127
         */
        byte b=10;
        System.out.println(b);
        System.out.println(Byte.MAX_VALUE);
        System.out.println(Byte.MIN_VALUE);
    }

    public static void main6(String[] args) {
       /*
       双精度浮点型
        */
        double d =12.5;
        System.out.println(d);
        System.out.println(Double.MAX_VALUE);
        System.out.println(Double.MIN_VALUE);

        float f=12.5f;
        //编译器认为12.5是double类型的数
        //所以要么f=12.5，要么f=(float)12.5
        System.out.println(f);

    }

    public static void main10(String[] args) {
        //字符型 两个字节
        char ch='a';
        System.out.println(ch);
    }

    public static void main1(String[] args) {
        boolean b=false;
        System.out.println(b);
        //特殊的boolean只分真假
    }

    public static void main15(String[] args) {
        int a =10;
        long b=100L;
        b=a;//可以通过编译  隐式类型转换
        a=(int)b;//不能进行直接赋值  强制类型转换-》 盖章 显示类型转换
        System.out.println(b);
        System.out.println("\\\"hello\\\"");
    }

}
0